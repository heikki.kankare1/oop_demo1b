import java.util.ArrayList;

public class oop_demo1b {
    public static void main(String[] args) {

    }
}

class Asiakas {
    private String tunniste;
    private String nimi;

    public Asiakas(String tunniste, String nimi) {
        this.tunniste = tunniste;
        this.nimi = nimi;
    }

    public String getTunniste() {
        return tunniste;
    }

    public void setTunniste(String tunniste) {
        this.tunniste = tunniste;
    }

    public String getNimi() {
        return nimi;
    }

    public void setNimi(String nimi) {
        this.nimi = nimi;
    }
}

class Esiintyja {
    private String tunniste;
    private String nimi;
    private String erikoisala;
    private double palkkio;

    public Esiintyja(String tunniste, String nimi, String erikoisala, double palkkio) {
        this.tunniste = tunniste;
        this.nimi = nimi;
        this.erikoisala = erikoisala;
        this.palkkio = palkkio;
    }

    public String getTunniste() {
        return tunniste;
    }

    public void setTunniste(String tunniste) {
        this.tunniste = tunniste;
    }

    public String getNimi() {
        return nimi;
    }

    public void setNimi(String nimi) {
        this.nimi = nimi;
    }

    public String getErikoisala() {
        return erikoisala;
    }

    public void setErikoisala(String erikoisala) {
        this.erikoisala = erikoisala;
    }

    public double getPalkkio() {
        return palkkio;
    }

    public void setPalkkio(double palkkio) {
        this.palkkio = palkkio;
    }
}

class Tilaisuus {
    private String tunniste;
    private String nimi;
    private String tyyppi;
    private Esiintyja esiintyja;
    private String paiva;

    public Tilaisuus(String tunniste, String nimi, String tyyppi, Esiintyja esiintyja, String paiva) {
        this.tunniste = tunniste;
        this.nimi = nimi;
        this.tyyppi = tyyppi;
        this.esiintyja = esiintyja;
        this.paiva = paiva;
    }

    public String getTunniste() {
        return tunniste;
    }

    public void setTunniste(String tunniste) {
        this.tunniste = tunniste;
    }

    public String getNimi() {
        return nimi;
    }

    public void setNimi(String nimi) {
        this.nimi = nimi;
    }

    public String getTyyppi() {
        return tyyppi;
    }

    public void setTyyppi(String tyyppi) {
        this.tyyppi = tyyppi;
    }

    public Esiintyja getEsiintyja() {
        return esiintyja;
    }

    public void setEsiintyja(Esiintyja esiintyja) {
        this.esiintyja = esiintyja;
    }

    public String getPaiva() {
        return paiva;
    }

    public void setPaiva(String paiva) {
        this.paiva = paiva;
    }
}


